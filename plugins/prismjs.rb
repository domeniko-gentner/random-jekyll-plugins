# Purpose:  Jekyll Plugin to prepare markdown for syntax highlighting with prism.js
# Usage:    {% prism language %} - See http://prismjs.com/#languages-list for valid short names
# Note:     This script transforms code to escaped html entities before it puts it back, so Kramdown does not get confused.
# Author:   Scorcher24 <scorcher24@gmail.com>
require 'cgi'

module Jekyll
    module Tags     
        class Prism < Liquid::Block
            def initialize(tag_name, text, tokens)
                @arg = text.strip
                super
            end

            def render(context)
                output = super(context)
                output = CGI.escapeHTML(output);
                "<pre><code class=\"language-#{@arg}\">#{output}</code></pre>"
            end
        end
    end
end 

Liquid::Template.register_tag('prism', Jekyll::Tags::Prism)

