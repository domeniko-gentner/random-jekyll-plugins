# Purpose:  Jekyll Plugin to display a html5 game/context via markdown
# Usage:    {% html5game foldername width height %}
# Note:     The script expects a index.html with the canvas to live in your webspace in a folder called 'games'. 
#           Replace this below if you have a different setup.
#           Example: /var/www/games/example would result in {% html5game example 256 256 %}
# Author:   Scorcher24 <scorcher24@gmail.com>
module Jekyll
    module Tags     
        class HTML5Game < Liquid::Tag
            def initialize(tag_name, markup, tokens)
                @arg = markup.gsub(/\s+/m, ' ').strip.split(" ")                
                super
            end

            def render(context)
                #output = super(context)
                site_url = context.registers[:site].config['url']                                           
                url = "#{site_url}/games/#{@arg[0]}/index.html" 
                
                "<div class=\"html5example\"><iframe frameborder=\"0\" scrolling=\"no\" width=\"#{@arg[1]}\" height=\"#{@arg[2]}\" src=\"#{url}\">.</iframe></div>"
            end
        end
    end
end 

Liquid::Template.register_tag('html5game', Jekyll::Tags::HTML5Game)

